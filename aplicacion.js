
class Persona {
    constructor(nombre,edad,fecha){
        this.nombre = nombre,
        this.edad = edad,
        this.fecha = fecha

    }
}

class Interface{

    listaPersonal(objPersona){
        
        let contenedorPersona = document.getElementById("listadousuario");
        let contenedor = document.createElement("div");

        contenedor.innerHTML =`
        
            <div class="card mb-3">
                <div class="card-body">
                    <strong>Nombre: </strong> ${objPersona.nombre}
                    <strong>Edad: </strong> ${objPersona.edad}
                    <strong>Fecha: </strong> ${objPersona.fecha}
                    <button class="btn btn-danger" id="Eliminar" name="delete">Eliminar</button> 
                </div>
            </div>
        

        `;

        contenedorPersona.appendChild(contenedor);
        

    }

    limpiarFormulario(){
        document.getElementById('nuevoPersonal').reset();
    }

    eliminarCard(valor){
        console.log('valor', valor)

        if (valor.name == "delete"){
            valor.parentElement.parentElement.parentElement.remove();
        }
        
    }





    
}

// DOM DEL NAVEGADOR

document.getElementById("nuevoPersonal").addEventListener('submit', function (e){

    e.preventDefault();

    let nombre = document.getElementById("nombre").value;
    let edad = document.getElementById("edad").value;
    let fecha = document.getElementById("FechaRegistro").value;

    let newPersona = new Persona(nombre,edad,fecha);
    let InterfacePersona = new Interface();
    InterfacePersona.listaPersonal(newPersona)
    InterfacePersona.limpiarFormulario();

})

document.getElementById("listadousuario").addEventListener('click', function (e){
    let eliminar = e.target;
    const interfacePersona = new Interface();
    interfacePersona.eliminarCard(eliminar);



})

    